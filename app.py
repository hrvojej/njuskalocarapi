from bs4 import BeautifulSoup
import requests
import urllib.request
from flask import Flask, request,render_template
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry
from flask import jsonify
import json
import pandas as pd

app = Flask(__name__)
app.config['JSON_AS_ASCII'] = False
app.config["TEMPLATES_AUTO_RELOAD"] = True

      
        
# Njuškalo
@app.route('/api/getAdFromNjuskalo/')
def api_url_Njuskalo():

    url = request.args.get('url')
    
    headers = [{'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'},
               {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'},
               {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/83.0.4103.116 Safari/537.36'},
               {'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:77.0) Gecko/20100101 Firefox/77.0'}]

    session = requests.Session()
    retry = Retry(connect=7, backoff_factor=0.5)
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)
    r = requests.get(url, headers=headers[0])
    soup = BeautifulSoup(r.text, "lxml")

    # Function for fetching all ad info from single page
    adID = ""
    adPublishedDate = ""
    adPublishedTime = ""
    adViewed = ""
    adUserType = ""
    adUserNoVerfiedIn = ""
    adUserLoc = ""
    adUserWeb = ""
    adUserEmail = ""
    adUserTelList = []
    carManufacturer = ""
    carModel = ""
    carType = ""
    carYear = ""
    carModelYear = ""
    carRegistration = ""
    carMileage = ""
    carFuel = ""
    carPower = ""
    carCCM = ""
    carGearType = ""
    carGearNo = ""
    carConsumption = ""
    carCO2 = ""
    carECOCategory = ""
    carCondition = ""
    carOwner = ""
    carServiceBook = ""
    carGaraged = ""
    carDescription = ""
    carPayment = ""
    carOther = ""
    carAdditionalEquipement = ""
    carSafety = ""
    carSecurity = ""
    carComfort = ""
    adCarLocation = []
    adImageURLs = []

    image_attributes = soup.findAll('li', attrs={'class': 'ClassifiedDetailGallery-sliderListItem--image'})
    for imgURL in image_attributes:
        adImageURLs.append(imgURL['data-large-image-url'])
    
    k = 0
    if soup.find('div', attrs={'class': 'ClassifiedDetailSummary-adCode'}):
        adID = soup.find('div', attrs={'class': 'ClassifiedDetailSummary-adCode'}).get_text().strip().replace('\n',' ').replace('\r', '').split(':')[1]
    if soup.findAll('dd', attrs={'class': 'ClassifiedDetailSystemDetails-listData'}):
        adPublishedDate, adPublishedTime = soup.find('dd', attrs={'class': 'ClassifiedDetailSystemDetails-listData'}).get_text().strip().replace('\n', ' ').replace('\r','').split('u')
    if len(soup.findAll('dd', attrs={'class': 'ClassifiedDetailSystemDetails-listData'}))>2:
        adViewed = soup.findAll('dd', attrs={'class': 'ClassifiedDetailSystemDetails-listData'})[2].get_text().strip().replace('\n',' ').replace('\r','').split(' ')[0]
    if soup.find('a', attrs={'class': 'ClassifiedDetailOwnerDetails-linkAllAds'}):
        adUserType = soup.find('a', attrs={'class': 'ClassifiedDetailOwnerDetails-linkAllAds'})
        if adUserType.get('href')[1:9].strip() == 'korisnik':
            adUserType = 'korisnik'
        else:
            adUserType = 'trgovina'

    if soup.find('i', attrs={'class': 'icon--classifiedDetailViewVerified'}):
        adUserNoVerfiedIn = soup.find('i', attrs={'class': 'icon--classifiedDetailViewVerified'}).find_parent(
            'div').get_text().strip().replace('\n', ' ').replace('\r', '').split(':')[1].strip()

    if soup.findAll('ul', attrs={'class': 'ClassifiedDetailOwnerDetails-contact'}):
        adSellerInfo = soup.findAll('ul', attrs={'class': 'ClassifiedDetailOwnerDetails-contact'})[1]

    if soup.find('i', attrs={'class': 'icon--classifiedDetailViewPin'}):
        if adSellerInfo.find('i', attrs={'class': 'icon--classifiedDetailViewPin'}).get_text().strip() == "Adresa:":
            adUserLoc = []
            adUserLoc = adSellerInfo.find('i', attrs={'class': 'icon--classifiedDetailViewPin'}).find_parent(
                'li').get_text().strip().replace('\n', ' ').replace('\r', '').split(':')[1].strip().split(",")

    if soup.find('i', attrs={'class': 'icon--classifiedDetailViewGlobe'}):
        if adSellerInfo.find('i',
                             attrs={'class': 'icon--classifiedDetailViewGlobe'}).get_text().strip() == "Web adresa:":
            adUserWeb = adSellerInfo.find('i', attrs={'class': 'icon--classifiedDetailViewGlobe'}).findNext('a')['href']

    if soup.find('i', attrs={'class': 'icon--classifiedDetailViewEmail'}):
        if adSellerInfo.find('i', attrs={'class': 'icon--classifiedDetailViewEmail'}).get_text().strip() == "E-mail:":
            adUserEmail = adSellerInfo.find('i', attrs={'class': 'icon--classifiedDetailViewEmail'}).findNext('a')[
                              'href'][7:]
    if soup.findAll('a', attrs={'class': 'link-tel'}):
        adUserTel = soup.findAll('a', attrs={'class': 'link-tel'})
        for a in adUserTel:
            adUserTelList.append(a['href'][5:])

    for dt in soup.select('dt[class*="ClassifiedDetailBasicDetails"]'):
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Lokacija vozila":
            adCarLocation = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '').strip().split(',')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Marka automobila":
            carManufacturer = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Model automobila":
            carModel = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Tip automobila":
            carType = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Godina proizvodnje":
            carYear = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '').replace(". godište", "")
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Godina modela":
            carModelYear = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '').replace(".", "")
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Registriran do":
            carRegistration = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '').replace(".", "")
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Prijeđeni kilometri":
            carMileage = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '').replace(" km", "")
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Motor":
            carFuel = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Snaga motora":
            carPower = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Radni obujam":
            carCCM = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Mjenjač":
            carGearType = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Broj stupnjeva":
            carGearNo = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Potrošnja goriva":
            carConsumption = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Maksimalna emisija CO2":
            carCO2 = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Ekološka kategorija vozila":
            carECOCategory = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Stanje":
            carCondition = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Vlasnik":
            carOwner = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Servisna knjiga":
            carServiceBook = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')
        if dt.get_text().strip().replace('\n', ' ').replace('\r', '') == "Garažiran":
            carGaraged = dt.findNext('dd').get_text().strip().replace('\n', ' ').replace('\r', '')

        # Opis oglasa
    opis = soup.find('div', attrs={'class': 'ClassifiedDetailDescription-text'})
    if opis:
        carDescription = opis.get_text().replace('\n', ' ').replace('\r', '')

        # Dodatne informacije
    for section in soup.select('section[class*="ClassifiedDetailPropertyGroups-group"]'):
        title = section.h3.get_text().strip().replace('\n', ' ')
        carUL = section.div.ul.get_text().strip().replace('\n', ' ').replace('\r', '').splitlines()[0]
        carUL = [i.strip() for i in carUL.split('  ') if i]
        if title == 'Mogućnost plaćanja':
            carPayment = carUL
        if title == 'Dodatni podaci':
            carOther = carUL
        if title == 'Dodatna oprema vozila':
            carAdditionalEquipement = carUL
        if title == 'Sigurnost':
            carSafety = carUL
        if title == 'Udobnost':
            carComfort = carUL
        if title == 'Sigurnost protiv krađe':
            carSecurity = carUL

    adFullFInfo = {
        u'adID': adID,
        u'adPublishedDate': adPublishedDate,
        u'adPublishedTime': adPublishedTime,
        u'adViewed': adViewed,
        u'adUserType': adUserType,
        u'adUserNoVerfiedIn': adUserNoVerfiedIn,
        u'adUserLoc': adUserLoc,
        u'adUserWeb': adUserWeb,
        u'adUserEmail': adUserEmail,
        u'adUserTelList': adUserTelList,
        u'carManufacturer': carManufacturer,
        u'carModel': carModel,
        u'carType': carType,
        u'carYear': carYear,
        u'carModelYear': carModelYear,
        u'carRegistration': carRegistration,
        u'carMileage': carMileage,
        u'carFuel': carFuel,
        u'carPower': carPower,
        u'carCCM': carCCM,
        u'carGearType': carGearType,
        u'carGearNo': carGearNo,
        u'carConsumption': carConsumption,
        u'carCO2': carCO2,
        u'carECOCategory': carECOCategory,
        u'carCondition': carCondition,
        u'carOwner': carOwner,
        u'carServiceBook': carServiceBook,
        u'carGaraged': carGaraged,
        u'carDescription': carDescription,
        u'carPayment': carPayment,
        u'carOther': carOther,
        u'carAdditionalEquipement': carAdditionalEquipement,
        u'carSafety': carSafety,
        u'carSecurity': carSecurity,
        u'carComfort': carComfort,
        u'adCarLocation': adCarLocation,
        u'adImageURLs': adImageURLs
    }
    return adFullFInfo

# /api/getAdFromNjuskalo/?url=https://www.njuskalo.hr/auti/mazda-cx-5-cd175-awd-revolution-19-alu-parking-senzori-kamera-oglas-30950305
            


        
@app.route('/api/getAdFromOglasnik/')
def api_url_Oglasnik():

        url = request.args.get('url')
    
        ### TESTING 
        # -----------------------------------------
        #Test parsing individual URLs
        #url = "https://www.oglasnik.hr/prodaja-automobila/vw-golf-vii-1-6-tdi-navi-pdc-parking-senzori-360-temp-reg-god-dana-oglas-4114559"
        r = requests.get(url)
        data = r.text
        soup = BeautifulSoup(data, "html.parser")

        # Check/Get Location, Price and Details
        #Check if alt = Location 
        check_location = soup.find('img', alt='Lokacija')
        if check_location!=None:
            car_ad_location = soup.find("div",  {"class": ["top-details"]}).p 
        else: car_ad_location = None
        if soup.find("span",  {"class": ["price-oglas-details"]}): 
            car_ad_price = soup.find("span",  {"class": ["price-oglas-details"]}) 
        else: car_ad_price = None
        if soup.findAll("span",  {"class": ["color-light"]}):
            car_ad_details = soup.findAll("span",  {"class": ["color-light"]})
        else: car_ad_details = None

        # Check if location/price exists
        results = []
        if car_ad_price==None: results[0:1] = ["Lokacija: Nebitna","cijena: Nema cijene"]
        else:
            if car_ad_location!= None:
                car_ad_location = str("Lokacija: ") + car_ad_location.text.replace('\xa0 ','').replace(",","")
                results[0:1]=[car_ad_location,car_ad_price.text]
            else:
                results[0:1] = ["Lokacija: Nema lokacije",car_ad_price.text]

        # Iterate over car details
        if car_ad_details!=None:
            for title in car_ad_details:
                full_title= title.text + title.next_sibling
                results.append(''.join(full_title).replace('\n',''))
            print(results)
        resultDict = dict((x.split(':')[0].strip(), x.split(':')[1].strip()) for x in results if ':' in x)

        return jsonify(resultDict)   
    

# /api/getAdFromOglasnik/?url=https://www.oglasnik.hr/prodaja-automobila/vw-golf-vii-1-6-tdi-navi-pdc-parking-senzori-360-temp-reg-god-dana-oglas-4114559

@app.route("/")
def main():
    return "Dohvat podataka sa Njuškala i Oglasnika"

if __name__ == '__main__':
    serve(app, 
        host='0.0.0.0', 
        port=80, 
        threads=16, 
        connection_limit=1000,
        cleanup_interval=15,
        channel_timeout=30,
        inbuf_overflow=52428800,
        outbuf_high_watermark=167772160,
        outbuf_overflow=10485760)



    
    

  
  
  